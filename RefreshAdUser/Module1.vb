﻿Imports System.DirectoryServices
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Module Module1

    Sub Main()
        Try
            Dim conn As New SqlConnection(My.Settings.connString)
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            
            Dim flag As Boolean = True


            For i As Integer = 0 To 3

                Dim com As String = ""

                Dim path As String = ""
                If i = 0 Then
                    path = "LDAP://pts.org.tw/ou=公共電視台,ou=hr,dc=pts,dc=org,dc=tw"
                    com = "公共電視台"
                ElseIf i = 1 Then
                    path = "LDAP://pts.org.tw/ou=原住民電視台,ou=hr,dc=pts,dc=org,dc=tw"
                    com = "原住民電視台"
                ElseIf i = 2 Then
                    path = "LDAP://pts.org.tw/ou=客家電視台,ou=hr,dc=pts,dc=org,dc=tw"
                    com = "客家電視台"
                ElseIf i = 3 Then
                    path = "LDAP://pts.org.tw/ou=台語頻道籌備中心,ou=hr,dc=pts,dc=org,dc=tw"
                    com = "台語頻道籌備中心"
                End If

                Dim oRoot1 As DirectoryEntry = New DirectoryEntry(path, "mamadmin", "mamadmin")
                'Dim oRoot1 As DirectoryEntry = New DirectoryEntry("LDAP://dc02.pts.org.tw/ou=原住民電視台,ou=hr,dc=pts,dc=org,dc=tw", "mamadmin", "mamadmin")
                Dim oSearcher1 As DirectorySearcher = New DirectorySearcher(oRoot1)
                Dim oResults1 As SearchResultCollection

                Dim name As String = ""
                Dim dept As String = ""
                Dim accountName As String = ""
                Dim displayName As String = ""
                Dim objClass As String = ""
                Dim mail As String = ""
                Dim distinguishedName As String = ""

                'oSearcher1.PropertiesToLoad.Add("name")
                oSearcher1.PropertiesToLoad.Add("department")
                oSearcher1.PropertiesToLoad.Add("samaccountname")
                oSearcher1.PropertiesToLoad.Add("displayname")
                oSearcher1.PropertiesToLoad.Add("mail")
                oSearcher1.PropertiesToLoad.Add("objectclass")
                oSearcher1.PropertiesToLoad.Add("distinguishedName")
                Try
                    oResults1 = oSearcher1.FindAll()

                    If (flag) Then
                        '更新FCACTIVE='N'
                        Dim cmd0 As New SqlCommand("Update TBUSERS Set FCACTIVE='N' Where FSPASSWD='//'", conn)
                        cmd0.ExecuteNonQuery()

                        flag = False
                    End If


                Catch ex As Exception
                    Exit Sub
                End Try




                Try
                    For Each oResult1 As SearchResult In oResults1


                        If oResult1.Properties("department").Count > 0 Then
                            dept = oResult1.Properties("department")(0)
                        Else
                            dept = ""
                        End If

                        If oResult1.Properties("samaccountname").Count > 0 Then
                            accountName = oResult1.Properties("samaccountname")(0)

                        Else
                            accountName = ""
                        End If

                        If oResult1.Properties("displayname").Count > 0 Then
                            displayName = oResult1.Properties("displayname")(0)

                        Else
                            displayName = ""
                        End If

                        If oResult1.Properties("mail").Count > 0 Then
                            mail = oResult1.Properties("mail")(0)
                        Else
                            mail = ""
                        End If

                        If oResult1.Properties("objectclass").Count > 1 Then
                            objClass = oResult1.Properties("objectclass")(1)
                        Else
                            objClass = ""
                        End If

                        If oResult1.Properties("distinguishedName").Count > 0 Then
                            distinguishedName = oResult1.Properties("distinguishedName")(0)
                        Else
                            distinguishedName = ""
                        End If

                        If objClass = "person" Then

                            Try

                                Dim com1 As String = ""
                                Dim dept1 As String = ""
                                Dim part1 As String = ""
                                Dim addG1 As Boolean = False
                                Dim addG2 As Boolean = False
                                Dim addG3 As Boolean = False

                                '判斷部門組別有無相同
                                If distinguishedName <> "" Then
                                    Dim ou() As String = distinguishedName.Split(",")
                                    If ou(3).ToString.Replace("OU=", "") = "HR" Then
                                        com1 = ou(2).ToString.Replace("OU=", "")
                                        dept1 = ou(1).ToString.Replace("OU=", "")
                                        part1 = ""

                                        Dim ou1 As String = com1 + "/" + dept1

                                        If (ou1 = "公共電視台/新媒體部/媒體資材組") Then
                                            addG1 = True
                                        End If
                                        If (ou1 = "公共電視台/新聞部") Or (ou1 = "公共電視台/國際部/國際頻道組") Or
                                                (ou1 = "原住民電視台/新聞部") Or (ou1 = "客家電視台/新聞部") Then
                                            addG2 = True
                                        End If
                                        If (ou1 = "公共電視台/新聞部/製作組") Then
                                            addG3 = True
                                        End If

                                        Dim da_dept As New SqlDataAdapter("Select FSDEPT From TBUSERS Where FSUSER_ID='" + accountName + "' And FSPASSWD='//'", conn)
                                        Dim ds_dept As New DataSet
                                        da_dept.Fill(ds_dept)
                                        If ds_dept.Tables(0) IsNot Nothing AndAlso ds_dept.Tables(0).Rows.Count > 0 Then
                                            If ds_dept.Tables(0).Rows(0).Item("FSDEPT").ToString = com1 + "/" + dept1 Then
                                                'Dim s As String = ""
                                            Else
                                                Dim cmd1 As New SqlCommand("Delete From TBUSERS Where FSUSER_ID='" + accountName + "'", conn)

                                                cmd1.ExecuteNonQuery()

                                                'Dim cmd2 As New SqlCommand("Delete From TBUSER_GROUP Where FSUSER_ID='" + accountName + "'", conn)

                                                'cmd2.ExecuteNonQuery()
                                            End If
                                        End If
                                    ElseIf ou(1).ToString.Replace("OU=", "") = "台語頻道籌備中心" Then
                                        com1 = ou(1).ToString.Replace("OU=", "")

                                        Dim da_dept As New SqlDataAdapter("Select FSDEPT From TBUSERS Where FSUSER_ID='" + accountName + "' And FSPASSWD='//'", conn)
                                        Dim ds_dept As New DataSet
                                        da_dept.Fill(ds_dept)
                                        If ds_dept.Tables(0) IsNot Nothing AndAlso ds_dept.Tables(0).Rows.Count > 0 Then
                                            If ds_dept.Tables(0).Rows(0).Item("FSDEPT").ToString = com1 Then
                                                'Dim s As String = ""
                                            Else
                                                Dim cmd1 As New SqlCommand("Delete From TBUSERS Where FSUSER_ID='" + accountName + "'", conn)

                                                cmd1.ExecuteNonQuery()

                                                'Dim cmd2 As New SqlCommand("Delete From TBUSER_GROUP Where FSUSER_ID='" + accountName + "'", conn)

                                                'cmd2.ExecuteNonQuery()
                                            End If
                                        End If

                                    Else
                                        com1 = ou(3).ToString.Replace("OU=", "")
                                        dept1 = ou(2).ToString.Replace("OU=", "")
                                        part1 = ou(1).ToString.Replace("OU=", "")


                                        Dim ou1 As String = com1 + "/" + dept1
                                        Dim ou2 As String = com1 + "/" + dept1 + "/" + part1

                                        If (ou2 = "公共電視台/新媒體部/媒體資材組") Then
                                            addG1 = True
                                        End If
                                        If (ou1 = "公共電視台/新聞部") Or (ou2 = "公共電視台/國際部/國際頻道組") Or
                                                (ou1 = "原住民電視台/新聞部") Or (ou1 = "客家電視台/新聞部") Then
                                            addG2 = True
                                        End If
                                        If (ou2 = "公共電視台/新聞部/製作組") Then
                                            addG3 = True
                                        End If


                                        Dim da_dept As New SqlDataAdapter("Select FSDEPT From TBUSERS Where FSUSER_ID='" + accountName + "' And FSPASSWD='//'", conn)
                                        Dim ds_dept As New DataSet
                                        da_dept.Fill(ds_dept)
                                        If ds_dept.Tables(0) IsNot Nothing AndAlso ds_dept.Tables(0).Rows.Count > 0 Then
                                            If ds_dept.Tables(0).Rows(0).Item("FSDEPT").ToString = com1 + "/" + dept1 + "/" + part1 Then
                                                'Dim s As String = ""
                                            Else
                                                Dim cmd1 As New SqlCommand("Delete From TBUSERS Where FSUSER_ID='" + accountName + "'", conn)

                                                cmd1.ExecuteNonQuery()

                                                'Dim cmd2 As New SqlCommand("Delete From TBUSER_GROUP Where FSUSER_ID='" + accountName + "'", conn)

                                                'cmd2.ExecuteNonQuery()
                                            End If
                                        End If


                                    End If
                                End If


                                Dim dis As String = displayName

                                Dim da As New SqlDataAdapter("Select FSUSER_ID From TBUSERS Where FSUSER_ID='" + accountName + "'", conn)
                                Dim ds As New DataSet
                                da.Fill(ds)
                                If ds.Tables(0) IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then

                                    If displayName.IndexOf("(") > -1 Then
                                        displayName = displayName.Substring(displayName.IndexOf("(") + 1, displayName.IndexOf(")") - displayName.IndexOf("(") - 1)
                                    End If

                                    If part1 = "" And dept1 <> "" Then
                                        Dim cmd1 As New SqlCommand("Update TBUSERS Set FSUSER=N'" + displayName + "',FCACTIVE='Y',FSDEPT='" + com1 + "/" + dept1 + "' Where FSUSER_ID='" + accountName + "'", conn)
                                        cmd1.ExecuteNonQuery()
                                    ElseIf part1 = "" And dept1 = "" Then
                                        Dim cmd1 As New SqlCommand("Update TBUSERS Set FSUSER=N'" + displayName + "',FCACTIVE='Y',FSDEPT='" + com1 + "' Where FSUSER_ID='" + accountName + "'", conn)
                                        cmd1.ExecuteNonQuery()
                                    Else
                                        Dim cmd1 As New SqlCommand("Update TBUSERS Set FSUSER=N'" + displayName + "',FCACTIVE='Y',FSDEPT='" + com1 + "/" + dept1 + "/" + part1 + "' Where FSUSER_ID='" + accountName + "'", conn)
                                        cmd1.ExecuteNonQuery()
                                    End If

                                Else
                                    If displayName.IndexOf("(") > -1 Then
                                        displayName = displayName.Substring(displayName.IndexOf("(") + 1, displayName.IndexOf(")") - displayName.IndexOf("(") - 1)
                                    End If

                                    If part1 = "" And dept1 <> "" Then
                                        Dim cmd1 As New SqlCommand("Insert Into TBUSERS Values('" + accountName + "',N'" + displayName +
                                                              "','//','" + com1 + "/" + dept1 + "','" + mail + "','Y','','_3TJimmy','" + Now.ToString("yyyy-MM-dd HH:mm:ss") +
                                                              "',null,null,1,null)", conn)

                                        cmd1.ExecuteNonQuery()
                                    ElseIf part1 = "" And dept1 = "" Then  'For 台語頻道籌備中心
                                        Dim cmd1 As New SqlCommand("Insert Into TBUSERS Values('" + accountName + "',N'" + displayName +
                                                "','//','" + com1 + "','" + mail + "','Y','','hsing1','" + Now.ToString("yyyy-MM-dd HH:mm:ss") +
                                                "',null,null,1,null)", conn)

                                        cmd1.ExecuteNonQuery()
                                    Else
                                        Dim cmd1 As New SqlCommand("Insert Into TBUSERS Values('" + accountName + "',N'" + displayName +
                                                              "','//','" + com1 + "/" + dept1 + "/" + part1 + "','" + mail + "','Y','','_3TJimmy','" + Now.ToString("yyyy-MM-dd HH:mm:ss") +
                                                              "',null,null,1,null)", conn)

                                        cmd1.ExecuteNonQuery()
                                    End If


                                    Dim cmd2 As New SqlCommand("Insert Into TBUSER_GROUP Values('" + accountName + "','Guest',null,'" + Now.ToString("yyyy-MM-dd HH:mm:ss") + "',null,null)", conn)

                                    cmd2.ExecuteNonQuery()


                                    If addG1 Then
                                        '判斷使用者帳號是否已在權限中
                                        Dim da_addG1 As New SqlDataAdapter("Select Count(*) As FNCOUNT From TBUSER_GROUP Where FSUSER_ID='" + accountName + "' And FSGROUP_ID='Librarain'", conn)
                                        Dim ds_aggG1 As New DataSet
                                        da_addG1.Fill(ds_aggG1)
                                        If (ds_aggG1.Tables(0) IsNot Nothing AndAlso ds_aggG1.Tables(0).Rows.Count > 0) Then
                                            If Integer.Parse(ds_aggG1.Tables(0).Rows(0).Item("FNCOUNT").ToString) = 0 Then
                                                Dim cmd As New SqlCommand("Insert Into TBUSER_GROUP Values('" + accountName + "','Librarain',null,'" + Now.ToString("yyyy-MM-dd HH:mm:ss") + "',null,null)", conn)

                                                cmd.ExecuteNonQuery()
                                            End If

                                        End If


                                    End If
                                    If addG2 Then
                                        Dim da_addG2 As New SqlDataAdapter("Select Count(*) As FNCOUNT From TBUSER_GROUP Where FSUSER_ID='" + accountName + "' And FSGROUP_ID='Authorized User'", conn)
                                        Dim ds_aggG2 As New DataSet
                                        da_addG2.Fill(ds_aggG2)
                                        If (ds_aggG2.Tables(0) IsNot Nothing AndAlso ds_aggG2.Tables(0).Rows.Count > 0) Then
                                            If Integer.Parse(ds_aggG2.Tables(0).Rows(0).Item("FNCOUNT").ToString) = 0 Then
                                                Dim cmd As New SqlCommand("Insert Into TBUSER_GROUP Values('" + accountName + "','Authorized User',null,'" + Now.ToString("yyyy-MM-dd HH:mm:ss") + "',null,null)", conn)

                                                cmd.ExecuteNonQuery()
                                            End If

                                        End If

                                    End If
                                    If addG3 Then
                                        Dim da_addG3 As New SqlDataAdapter("Select Count(*) As FNCOUNT From TBUSER_GROUP Where FSUSER_ID='" + accountName + "' And FSGROUP_ID='NIC Group'", conn)
                                        Dim ds_aggG3 As New DataSet
                                        da_addG3.Fill(ds_aggG3)
                                        If (ds_aggG3.Tables(0) IsNot Nothing AndAlso ds_aggG3.Tables(0).Rows.Count > 0) Then
                                            If Integer.Parse(ds_aggG3.Tables(0).Rows(0).Item("FNCOUNT").ToString) = 0 Then
                                                Dim cmd As New SqlCommand("Insert Into TBUSER_GROUP Values('" + accountName + "','NIC Group',null,'" + Now.ToString("yyyy-MM-dd HH:mm:ss") + "',null,null)", conn)

                                                cmd.ExecuteNonQuery()
                                            End If

                                        End If

                                    End If
                                End If
                            Catch ex As Exception
                                File.AppendAllText("D:\ADError.txt", ex.Message + "_1" + vbNewLine)
                            End Try
                        End If


                        'System.IO.File.AppendAllText("D:\test.txt", name + vbNewLine)

                    Next


                Catch ex As Exception

                    File.AppendAllText("D:\ADError.txt", ex.Message + "_2" + vbNewLine)
                End Try

            Next
            conn.Close()
            conn.Dispose()
        Catch ex As Exception
            File.AppendAllText("D:\ADError.txt", ex.Message + "_3" + vbNewLine)
        End Try
        


    End Sub


End Module
